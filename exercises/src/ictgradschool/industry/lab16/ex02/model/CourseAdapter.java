package ictgradschool.industry.lab16.ex02.model;


import javax.swing.*;
import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {

	Course _courseModel;
	/**********************************************************************
	 * YOUR CODE HERE
	 */


	public CourseAdapter(Course _courseModel){
		_courseModel.addCourseListener(this);
		this._courseModel = _courseModel;
	}


/*	public CourseAdapter(Course _courseModel,JTable jTable){
		_courseModel.addCourseListener(this);
		this._courseModel = _courseModel;
		this.jTable = jTable;
	}*/

	@Override
	public void courseHasChanged(Course course) {
		System.out.println("Changing table");
		fireTableDataChanged();
	}

	@Override
	public int getRowCount() {
		return _courseModel.size();
	}

	@Override
	public int getColumnCount() {
		return 7;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex){
			case 0:
				return (Integer)(_courseModel.getResultAt(rowIndex)._studentID);
			case 1:
				return _courseModel.getResultAt(rowIndex)._studentSurname;
			case 2:
				return _courseModel.getResultAt(rowIndex)._studentForename;
			case 3:
				return _courseModel.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Exam);
			case 4:
				return _courseModel.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Test);
			case 5:
				return _courseModel.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Assignment);
			case 6:
				return _courseModel.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Overall);
		}

		return _courseModel.getResultAt(rowIndex);
	}

	public String getColumnName(int column){
		switch(column){
			case 0:
				return "Student ID";
			case 1:
				return "Surname";
			case 2:
				return "Forename";
			case 3:
				return "Exam";
			case 4:
				return "Test";
			case 5:
				return "Assignment";
			case 6:
				return "Overall";
		}
		return null;
	}
}