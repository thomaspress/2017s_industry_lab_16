package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener{

	StatisticsPanel statisticsPanel;

	/**********************************************************************
	 * YOUR CODE HERE
	 */
//	course.addListener
	public StatisticsPanelAdapter(StatisticsPanel statisticsPanel){
//		_courseModel.addCourseListener(this);
		this.statisticsPanel = statisticsPanel;
	}

	@Override
	public void courseHasChanged(Course course) {
		System.out.println("Changing stats panel");
		//statisticsPanel = new StatisticsPanel(course);
		statisticsPanel.repaint();
	}
	
}
