package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

public class DistributionPanelAdapter implements CourseListener{

	private DistributionPanel distributionPanel;
	/**********************************************************************
	 * YOUR CODE HERE
	 */

	public DistributionPanelAdapter(DistributionPanel distributionPanel){
//		_courseModel.addCourseListener(this);
		this.distributionPanel = distributionPanel;
	}

	@Override
	public void courseHasChanged(Course course) {
		System.out.println("Changing dist panel");
		//repaint with new results
		//distributionPanel = new DistributionPanel(course);
		distributionPanel.repaint();
	}
}
